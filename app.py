import random 
import requests as rq 
import pandas as pd 
import config 
import csv
import numpy as np 
import json
# import stats
import os.path
import matplotlib.pyplot as plt 
import seaborn as sns
plt.figure(num=None, figsize=(8, 6), dpi=80, facecolor='w', edgecolor='k')
%matplotlib inline

# Get choices
OPTIONS = rq.get(config.URL).json()
choices = OPTIONS[config.bandit]["choices"]

# Check if data file is empty; else create 
if os.path.exists(config.dataFile):
    df = pd.DataFrame.from_csv(config.dataFile)  
else:
    df = pd.DataFrame({'bandit': [], 'arm': [], 'status': [], 'value': []})

def pullArm(bandit, option, df):
    # Pull dem triggaaaa
    OPTIONS = rq.get(config.URL).json()
    choices = OPTIONS[bandit]["choices"]
    url= ("{}/{}/{}/{}/{}".format(config.URL, bandit, config.username, config.password, option))
    response  = rq.get(url).json()

    # Append to df 
    response['bandit'] = bandit 
    response['arm'] = option
    # df.loc[len(df)] = response
    df2 = pd.DataFrame({'bandit': [response['bandit']], 'arm': [response['arm']], 'status': [response['status']], 'value': [response['value']]})
    df = df.append(df2, ignore_index=True)   

    # Write to csv 
    df.to_csv(config.dataFile)

    return df

def cleanData():
    filename = config.dataFile
    f = open(filename, "w+")
    f.close()

def drawSample(sampleSize, df):
    for choice in choices:
        print("Sampling: putting " + str(sampleSize) + " coins in " + str(config.bandit) + " on handle " + choice)
        for _ in " "*sampleSize:  
            df = pullArm(config.bandit, choice, df)

sampleSize = 0
coinAmount = 2000
 
# EXECUTE
print("=== Welkom to Niek's Money Machine 1.0====")
print("PREPARE TO GET RICH")
print("**************************")

drawSample(sampleSize, df)

# Make decision
for _ in " "*coinAmount: 
    # Create distribution and determine probabililty 
    stats = df.groupby('arm')['value'].agg(['mean', 'std'])

    max_sample = 0
    for (arm,mean,std) in stats.itertuples(): 
        if(np.random.normal(mean, std, 1)[0]>max_sample):
            max_arm = arm
    decision = max_arm

    df = pullArm(config.bandit, decision, df)
    print("DUMP 1 COIN in " + str(config.bandit) + " on handle " + str(decision))

print("THANK YOU --- ENJOY ALL THOSE COINS-----")

# Subset dataframe 
# df1 = df[['bandit','arm', 'value']]
# df1.groupby('arm').describe()

# a = df1.groupby('arm').mean()